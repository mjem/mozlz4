#!/usr/bin/env python3

import stat
from pathlib import Path

from invoke import task

ACTIVATE_PATH = Path('activate')
ENV_PATH = Path('env')
ENV_ACTIVATE_PATH = ENV_PATH.joinpath('bin', 'activate')


@task
def activate(c):
	"""Create an `activate` script."""
	here = Path(__file__).parent
	has_env = ENV_PATH.exists()
	with ACTIVATE_PATH.open('w') as h:
		h.write("""#!/bin/bash
{env}export PATH={here}/bin:$PATH
""".format(env='source {here}/{envact}\n'.format(here=here, envact=ENV_ACTIVATE_PATH),
		   here=here))
	ACTIVATE_PATH.chmod(ACTIVATE_PATH.stat().st_mode | stat.S_IEXEC)
	print('Wrote {outname} ({bb} bytes)'.format(outname=ACTIVATE_PATH,
												bb=ACTIVATE_PATH.stat().st_size))


@task
def thirdparty(c):
	"""Recreate any 3rd-party files."""
	thirdparty_download(c)
	thirdparty_build(c)


@task
def thirdparty_download(c):
	"""Download any 3rd-party files used in the project."""
	c.run('wget https://www.gnu.org/licenses/gpl-3.0.md')


@task
def thirdparty_build(c):
	"""Create and derived files from 3rd-party inputs."""
	c.run('pandoc gpl-3.0.md -o LICENSE.rst')


@task
def lint_python(c):
	"""Run static and docstring tests of python code."""
	c.run('flake8 *.py')
	c.run('python3 -m doctest *.py')


@task
def lint_rst(c):
	"""Check restructuredText files."""
	c.run('restructuredtext-lint README.rst')
	c.run('restructuredtext-lint LICENSE.rst')


@task
def lint(c):
	"""Perform any code and doc checks."""
	lint_python(c)  # pylint, pycodestyle, pydocstyle, the other one
	lint_rst(c)  # rst-lint, rsttohtml
	lint_language(c)  # vale, others?

@task
def doc(c):
	c.run('rst2html5.py README.rst README.html')
	c.run('rst2html5.py LICENSE.rst LICENSE.html')
	# automatically build literate functional tests

@task
def test(c):
	"""Run automated tests."""
	c.run('pytest tests')
	# doctest
