#!/usr/bin/env python3

"""Display firefox configuration by reading profile values."""

import os
import sys
import json
import logging
import argparse
from pathlib import Path
import configparser

import lz4.block
from tabulate import tabulate

logger = logging.getLogger()

def expand(self):
	"""Patch pathlib.Path with a useful expand function.

	~ -> current user home directory
	~USER -> home directory of USER
	$ENV -> value of environment variable ENV
	"""
	return self.__class__(
		os.path.expanduser(
			os.path.expandvars(
				str(self))))
Path.expand = expand

NEWLINE = '\n'

DEFAULT_APP = Path('~/.mozilla/firefox').expand()
# DEFAULT_ORG = Path('~/.mozilla').expand()
DEFAULT_PROFILES_INI = Path('profiles.ini')
SECTION_PROFILE = 'Profile'
RECOVERY_FILENAME = Path('sessionstore-backups', 'recovery.jsonlz4')

def init_log():
	"""Initialise logging system to simple print to stdout."""
	logging.basicConfig(
		level=logging.DEBUG,
		handlers=[logging.StreamHandler()])


def argparse_path(exists=False, is_dir=False):
	"""argparse datatype for Pathlib paths."""
	def imp(s):
		result = Path(s)
		if exists and not result.exists():
			raise argparse.ArgumentTypeError('Argument {s} does not exist'.format(s=s))

		if is_dir and not result.is_dir():
			raise argparse.ArgumentTypeError('Argument {s} exists but is not a directory'.format(s=s))

		return Path(s)

	return imp


def mozlz4_to_text(path):
    """Read a Mozilla `jsonlz4` format file and return plain text contents."""
    bytestream = path.open('rb')
    bytestream.read(8)  # skip past the b"mozLz40\0" header
    valid_bytes = bytestream.read()
    text = lz4.block.decompress(valid_bytes)
    return text


class Profile:
	def __init__(self, name, path, default):
		self.name = name
		self.path = path
		self.default = default
		self.windows = None


class Tab:
	def __init__(self, url, title):
		self.url = url
		self.title = title


def get_profiles(path):
	logger.info('Reading profile {p}'.format(p=path))
	config = configparser.ConfigParser()
	config.read(path)
	for section in config.sections():
		if section.startswith(SECTION_PROFILE):
			name = None
			default = False
			path = None
			for k, v in config.items(section):
				if k == 'name':
					name = v

				elif k == 'path':
					path = v

				elif k == 'default':
					default = True

			yield Profile(name=name, path=path, default=default)


def list_profiles(profiles, target=sys.stdout):
	"""Show a table summarising all profiles."""
	rows = []
	for profile in profiles:
		rows.append(('Yes' if profile.default else '',
					 profile.name,
					 profile.path,
					 ' '.join('({c})'.format(c=len(w)) for w in profile.windows)))


	target.write(tabulate(rows, headers=('Default', 'Name', 'Path', 'Windows (tabs)')) + NEWLINE)


def read_tabs(app_path, profile):
	"""Populate the `windows` member of `profile`.

	Will be a list (one item per window) of tabs (string, most recent URL)."""
	path = app_path.joinpath(profile.path, RECOVERY_FILENAME)
	recovery = json.loads(mozlz4_to_text(path))
	profile.windows = []
	for window in recovery['windows']:
		# profile.windows.append([t['entries'][-1]['url'] for t in window['tabs']])
		# for tab in window['tabs']:
		profile.windows.append(
			[Tab(url=t['entries'][-1]['url'],
				 title=t['entries'][-1]['title']) for t in window['tabs']])
	# print(recovery['windows'][0]['tabs'][0]['entries'][0].keys())
	# logger.info('Profile {p} windows {w}'.format(
		# p=profile.name,
		# w=' '.join('({c})'.format(c=len(w)) for w in profile.windows)))


def list_tabs(profiles, target=sys.stdout):
	"""Show hierachical lists of all tabs."""
	indent = '  '
	for profile in profiles:
		target.write('Profile {p}\n'.format(p=profile.name))
		for window_num, window in enumerate(profile.windows, 0):
			target.write('{i}Window {w}\n'.format(i=indent, w=window_num))
			for tab in window:
				target.write('{i}{t}\n'.format(i=indent*2, t=tab.url))


def html_tabs(profiles, target=sys.stdout):
	"""Make HTML page of all profiles, windows and tabs."""
	target.write('<html>\n\t<body>\n')
	for profile in profiles:
		target.write('\t\t<h1>Profile {p}</h1>\n'.format(p=profile.name))
		for window_num, window in enumerate(profile.windows, 0):
			target.write('\t\t\t<h2>Window {w}</h2>\n\t\t\t<ul>\n'.format(w=window_num))
			for tab in window:
				target.write('\t\t\t\t<li><a href="{url}">{title}</a></li>\n'.format(
					url=tab.url, title=tab.title))

			target.write('\t\t\t</ul>\n')

	target.write('\t</body>\n</html>\n')


def main():
	"""Command line input."""
	init_log()
	parser = argparse.ArgumentParser()
	parser.add_argument('--list-profiles',
					  help='List all firefox profiles',
					  action='store_true')
	parser.add_argument('INPUT',
						nargs='?',
					  help='Process given input file')
	# parser.add_argument('--org-path',
						# default=DEFAULT_ORG,
						# type=argparse_path(exists=True, is_dir=True),
						# help='Location of top level mozilla directory')
	parser.add_argument('--app-path',
						default=DEFAULT_APP,
						type=argparse_path(exists=True, is_dir=True),
						 help='Subdirectory for application within org directory')
	parser.add_argument('--profile-path',
						default=DEFAULT_PROFILES_INI,
						type=argparse_path(),
						help='Filename of profiles.ini file')
	parser.add_argument('--tabs',
						action='store_true',
						help='Show URLs of all tabs (and titles if HTML output selected)')
	parser.add_argument('--out', '-o',
						help='Write to output file',
						type=argparse_path())
	parser.add_argument('--html',
						action='store_true',
						help='Output tabs in HTML format')
	args = parser.parse_args()

	if args.INPUT:
		sys.stdout.write((mozlz4_to_text(args.INPUT).decode('utf-8')) + NEWLINE)
		parser.exit()

	path = args.app_path.joinpath(args.profile_path)
	profiles = list(get_profiles(path))

	path = args.app_path.joinpath(args.app_path)
	for profile in profiles:
		read_tabs(path, profile)

	if args.list_profiles:
		list_profiles(profiles)
		parser.exit()

	output = args.out.open('w') if args.out else sys.stdout

	if args.tabs:
		if args.html:
			html_tabs(profiles, output)

		else:
			list_tabs(profiles, output)

		parser.exit()

	parser.exit('No actions specified')

if __name__ == '__main__':
	# main()
	import pdb
	try:
		main()
	except Exception as e:
		print(e)
		# pdb.set_trace()
		pdb.post_mortem()
