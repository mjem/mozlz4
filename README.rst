mozlz4
======

Introduction
------------

This is a tool to examine your Mozilla Firefox session store (the same database used to recover tabs after a crash or reload) and list all open tabs, either as plain text or an HTML document.

Installation and quick start
----------------------------

Clone the repository:

.. code:: bash

	git clone http://gitlab.com/mjem/mozlz4

Make a virtual environment:

.. code:: bash

	cd mozlz4
	python3 -m venv env
	. env/bin/activate
	pip3 install -r requirements.txt

Make an activation script for this project and run it:

.. code:: bash

	invoke activate
	. ./activate

Now test the tool:

.. code:: bash

	mozlz4 --list-profiles

Result:

.. code:: text

	Default    Name     Path              Windows (tabs)
	---------  -------  ----------------  ----------------
	           me       46126467.default  (79) (12)
	           videos   26473568.videos   (134)
	           music    56724576.music    (57)
	Yes        default  26372373.default  (3)
	           work     26368434.work     (2)

Text output
-----------

To see all profiles, windows and tabs in the session store displayed in the terminal:

.. code:: bash

	mozlz4 --tabs

HTML output
-----------

To create an HTML page listing all profiles, windows and tabs:

.. code:: bash

	mozlz4 --tabs --html --output mytabs.html

License and copyright
---------------------

The project is copyright Mike Elson 2019 and released under the GPL v3.0.
